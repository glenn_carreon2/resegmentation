import os
import shutil
import json


def test_func(x):
    y = x*2
    return y

def create_output_dir():
    '''

    :return: Creates output directory. If directory already exists, it deletes the old one and creates a new one
    '''
    output = os.path.join(os.getcwd() + r'\Output') # == (Current Working Directory)\Output
    if os.path.exists(output):
        shutil.rmtree(output) # if output directory exists, this statements delete it
    os.mkdir(output) #creates the output directory using (Current Working Directory)\Output

    return output


def list_input_dir(input_dir):
    '''
    :param input_dir: input directory provided by user in config.json
    :return: dictionary as {folder: [scan path]}
    '''

    casescanDir = {}   #{folder: [scan]}
    attachmentDirs = []


    for folder in os.listdir(input_dir): #iterating through case input directory and adding attachments
        attachmentDir = os.path.join(input_dir, folder, "Attachments") #problem is path is too specific, its hard to be flexible. What if there is no "Attachments" folder?
        attachmentDirs.append(attachmentDir)
        print(attachmentDirs)

        for attachmentDir in attachmentDirs:
            abs_paths = []
            cases = os.listdir(attachmentDir)
            for scan in cases:
                if scan.endswith('.ctm') or scan.endswith('.stl') or scan.endswith('.ply'):
                    z = (os.path.join(attachmentDir, scan))
                    abs_paths.append(z)
            casescanDir[folder] = abs_paths


    return casescanDir





def create_output_cases(input_dir, out_path, scan_path):
    for cases in (os.listdir(input_dir)):
        output = os.path.join(out_path, cases)
        os.mkdir(output)

    #for scans in output:
        #if output ==
#def wave_script(): #creates a txt file for wave command script for screenshot


"""
next funct after list dir walk through the returned dict and create output dir with dict key

dic = {folder name : [of scan paths]}

for f in dic:
    



"""